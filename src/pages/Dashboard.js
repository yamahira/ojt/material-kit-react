import {Helmet} from 'react-helmet';
import { Box, Container, Grid } from '@material-ui/core'
import Budget from 'src/components/dashboard/Budget'
import TasksProgress from 'src/components/dashboard//TasksProgress';
import TotalCustomers from 'src/components/dashboard//TotalCustomers';
import TotalProfit from 'src/components/dashboard//TotalProfit';

const Dashboard = () => (
  <>
   <Helmet>
       <title>Dashboard</title>
   </Helmet>
   <Box sx={{ minHeight: '100%', py: 3 }}>
    <Container>
        <Grid container spacing={3}>
            <Grid lg={3} sm={6} xl={3} xs={12} item>
                <Budget />
            </Grid>
            <Grid lg={3} sm={6} xl={3} xs={12} item>
                <TotalCustomers />
            </Grid>
            <Grid lg={3} sm={6} xl={3} xs={12} item>
                <TasksProgress />
            </Grid>
            <Grid lg={3} sm={6} xl={3} xs={12} item>
                <TotalProfit />
            </Grid>
        </Grid>
    </Container>
   </Box>
  </>
  );

export default Dashboard;
